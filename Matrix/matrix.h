//matrix V0.931
//template class with basic operations 
//feel free to use as you see fit
//Any questions, suggestions, requests please direct to mkasprowskik9@gmail.com
//by Marek Kasprowski

//TODO: Add unittest, add scalar multiplication, create reference

#pragma once
#include <iostream>
#include <fstream>
template <class type>class matrix
{
protected:
	//data matrix
	type **val;
	//matrix dimensions x = number of columns, y = number of rows
	unsigned int sizex;
	unsigned int sizey;
	//last error 
	int ef;
	//free val[][] memory
	bool clval();

public:
	//no val assigned null size, setsize before using
	matrix();
	//new identity matrix with size n
	matrix(unsigned int n);
	//new matrix with size x columns,y rows, val memory allocated, ready to use
	matrix(unsigned int x, unsigned int y);
	//copy matrix A
	matrix(matrix<type> &A);

	~matrix();
	
	//allocate new matrix with [y][x] size
	bool setsize(unsigned int x, unsigned int y);
	//assign value a to matrix
	bool assignval(type a);
	//copy values of matrix B to (this) matrix
	bool assignmatrix(matrix<type> &B);
	//swap content of A and B
	bool swap(matrix<type>&A, matrix<type>&B);
	//compare matrix B with A
	bool is_equal(matrix<type> &B);
	//set value v at [y][x] in matrix, performs range check
	bool setval(unsigned int x, unsigned int y,type v);
	//set value v at [y][x] in matrix, NO range check
	bool set(unsigned int x, unsigned int y, type v);
	//get value form [y][x] in matrix, performs range check
	type get(unsigned int x, unsigned int y);
	//get value form [y][x] in matrix, NO range check
	type at(unsigned int x, unsigned int y);
	
	unsigned int getsize_x()const;
	unsigned int getsize_y()const;

	//transpose matrix
	bool transpose();
	//create transposed matrix
	matrix<type> t();
	//inverse matrix
	bool inverse();
	//create inversed matrix 
	matrix<type> inv();
	//swap columns [c1] and [c2]
	bool c_swap(unsigned int c1,unsigned int c2);
	//swap rows [w1] and [w2]
	bool r_swap(unsigned int r1,unsigned int r2);

	//add values of matrix B to (this) matrix
	bool add(matrix<type>&B);
	//substract values of matrix B from (this) matrix
	bool sub(matrix<type>&B);
	//sum of matrix A and matrix B 
	matrix<type> add(matrix<type>&A,matrix<type>&B);
	//difference of matrix A and matrix B
	matrix<type> dif(matrix<type>&A, matrix<type>&B);
	//divide matrix by a
	matrix<type> div(type a);
	//multiplay matrix by a
	matrix<type> mul(type a);
	//multiplay matrix A and B
	matrix<type> mul(matrix<type>&A, matrix<type> &B);
	//multiplay matrix A by B
	bool mul(matrix<type> &B);

	//write to std output
	bool out();
	//file io operations
	bool fin(const char *filename);
	bool fout(const char *filename);

	bool operator ==(matrix<type>&B)
	{
		return is_equal(B);
	}
	bool operator = (type a)
	{
		return(assignval(a));
	}
	bool operator =(matrix<type> &B)
	{
		return (assignmatrix(B));
	}
	bool operator ||(matrix<type>&B)
	{
		return swap(*this, B);
	}

	bool operator +=(matrix<type>&B)
	{
		return add(B);
	}
	bool operator -=(matrix<type>&B)
	{
		return sub(B)
	}
	bool operator *=(matrix<type>&B)
	{
		return mul(B);
	}

	matrix<type> operator +(matrix<type>&B)
	{
		return add(*this, B);
	}
	matrix<type> operator -(matrix<type>&B)
	{
		return dif(*this, B);
	}
	matrix<type> operator *(matrix<type>&B)
	{
		return mul(*this, B);
	}
	matrix<type> operator *(type a)
	{
		return mul(a);
	}
	matrix<type> operator /(type a)
	{
		return	div(a);
	}

	bool operator >> (const char * filename)
	{
		return fout(filename);
	}
	bool operator <<(const char * filename)
	{
		return fin(filename);
	}

	int geterror();
	
	//most common types
	typedef matrix<int> matrixi;
	typedef matrix<unsigned int> matrixu;
	typedef matrix<float> matrixf;
	typedef matrix<double> matrixd;
};


#define NO_SIZE -1
#define BAD_SIZE -2
#define BAD_MEM -201
#define BAD_ACCESS 101

template<class type> matrix<type>::matrix()
{
	val = nullptr;
	ef = NO_SIZE;
	sizex = 0;
	sizey = 0;
}

template<class type> matrix<type>::matrix(unsigned int n)
{
	val = nullptr;
	setsize(n, n);
	assignval(0);
	for (int i = 0; i < n; i++)
		set(i, i, 1);
}

template<class type> matrix<type>::matrix(unsigned int x, unsigned int y)
{
	val = nullptr;
	ef = 0;
	setsize(x, y);
}

template<class type> matrix<type>::matrix(matrix<type> &A)
{
	val = nullptr;
	setsize(A.sizex, A.sizey);
	for (int i = 0; i < this->sizey; i++)
		for (int j = 0; j < this->sizex; j++)
			this->val[i][j] = A.val[i][j];
}

template<class type> matrix<type>::~matrix()
{
	clval();
}


template<class type> bool matrix<type>::clval()
{
	for (int i = 0; i < sizey; i++)
	{
		delete[] val[i];
		//val[i] = nullptr;
	}
	if (sizex < 0 || sizey < 0)
	{
		ef = BAD_MEM;
		sizex = 0;
		sizey = 0;
	}
	delete[] val;
	val = nullptr;
	return true;
}

template<class type> bool matrix<type>::setsize(unsigned int x, unsigned int y)
{
	if (val) clval();
	if ((x < 0) || (y < 0))
	{
		ef = BAD_SIZE;
		return false;
	}
	sizex = x;
	sizey = y;
	val = new type*[sizey];
	for (int i = 0; i < sizey; i++)
		val[i] = new type[sizex];
	ef = 0;
	return true;
}

template<class type> bool matrix<type>::setval(unsigned int x, unsigned int y, type v)
{
	if (x < sizex && y < sizey && x >= 0 && y >= 0 )
	{
		val[y][x] = v;
		return true;
	}
	ef = BAD_ACCESS;
	return false;
}

template<class type> bool matrix<type>::set(unsigned int x, unsigned int y, type v)
{
	val[y][x] = v;
	return true;
}


template<class type> type matrix<type>::get(unsigned int x, unsigned int y)
{
	if (x < sizex && y < sizey && x >= 0 && y >= 0) return val[y][x];
	ef = BAD_ACCESS;
	return (type)NAN;
}

template<class type> type matrix<type>::at(unsigned int x, unsigned int y)
{
	return val[y][x];
}

template<class type>inline unsigned int matrix<type>::getsize_x() const
{
	return this->sizex;
}

template<class type>inline unsigned int matrix<type>::getsize_y() const
{
	return this->sizey;
}

template<class type> bool matrix<type>::transpose()
{
	*this = this->t();
	return true;
}

template<class type> matrix<type> matrix<type>::t()
{
	matrix<type> T(this->sizey,this->sizex);
	for (int i = 0; i < this->sizey; i++)
		for (int j = 0; j < this->sizex; j++)
			T.val[j][i] = this->val[i][j];
	return matrix<type>(T);
}

template<class type> bool matrix<type>::inverse()
{
	if (sizex != sizey) return false;
	*this = this->inv();
	return true;
}

template<class type> matrix<type> matrix<type>::inv()
{
	if (sizex != sizey) return matrix<type>();
	matrix<type> N(*this);
	matrix<type> I(N.sizex);
	
	
	for (int i = 0; i < N.sizey; i++)
	{
		
		type n= N.val[i][i];
		if (n == 0)return matrix<type>();
		for (int x = N.sizex - 1; x >= 0; x--)
		{
			I.val[i][x] /= n;
			N.val[i][x] /= n;
			
		}
		
		for (int j = i+1; j < N.sizex; j++)
		{
			type d = N.val[j][i] / N.val[i][i]; 
			for (int x = N.sizex - 1; x >= 0; x--) 
			{
				N.val[j][x] -= d*N.val[i][x];
				I.val[j][x] -= d*I.val[i][x];
			}
			
			
		}
	}

	for (int i = N.sizey-1; i >0; i--)
	{
		for (int j = i-1; j >= 0; j--) 
		{
			type d = N.val[j][i]/ N.val[i][i];
			N.val[j][i] -= d*N.val[i][i];
			for (int x = N.sizex - 1; x >= 0; x--)
			{
				I.val[j][x] -= d*I.val[i][x];
			}

			
		}
	}
	return matrix<type>(I);
}

template<class type> bool matrix<type>::c_swap(unsigned int c1,unsigned int c2)
{
	if (c1 < 0 || c2 < 0) return false;
	if (c1 >= sizex || c2 >= sizex) return false;
	if (c1 == c2) return true;
	type temp;
	for (int i = 0; i < sizey; i++)
	{
		temp = val[i][c1];
		val[i][c1] = val[i][c2];
		val[i][c2] = temp;
	}
	return true;
}

template<class type> bool matrix<type>::r_swap(unsigned int r1,unsigned int r2)
{
	if (r1 < 0 || r2 < 0) return false;
	if (r1 >= sizey || r2 >= sizey) return false;
	if (r1 == r2) return true;
	type temp;
	for (int i = 0; i < sizex; i++)
	{
		temp = val[r1][i];
		val[r1][i] = val[r2][i];
		val[r2][i] = temp;
	}
	return true;
}

template<class type>bool matrix<type>::is_equal(matrix<type> &B)
{
	if ((sizex != B.sizex) || (sizey != B.sizey)) return false;
	for (int i = 0; i < sizey; i++)
		for (int j = 0; j < sizex; j++)
			if (val[i][j] != B.val[i][j]) return false;
	return true;
}


template<class type> bool matrix<type>::assignval(type a)
{
	if (ef < 0)	return false;
	for (int i = 0; i < sizey; i++)
		for (int j = 0; j < sizex; j++)
			val[i][j] = a;
	return true;
}

template<class type> bool matrix<type>::assignmatrix(matrix<type> &B)
{
	if (this->sizex != B.sizex || this->sizey != B.sizey) this->setsize(B.sizex, B.sizey);

	for (int i = 0; i < this->sizey; i++)
		for (int j = 0; j < this->sizex; j++)
			this->val[i][j] = B.val[i][j];
	return true;
}

template<class type> bool matrix<type>::swap(matrix<type>& A, matrix<type>& B)
{
	type **temp = A.val;
	unsigned int x = A.sizex;
	unsigned int y = A.sizey;
	A.val = B.val;
	A.sizex = B.sizex;
	A.sizey = B.sizey;
	B.val = temp;
	B.sizex = x;
	B.sizey = y;
	temp = nullptr;
	return true;
}

template<class type> bool matrix<type>::add(matrix<type>& B)
{
	if ((sizex != B.sizex) || (sizey != B.sizey)) return false;

	for (int i = 0; i < this->sizey; i++)
		for (int j = 0; j < this->sizex; j++)
			this->val[i][j] += B.val[i][j];
	return true;
}

template<class type> bool matrix<type>::sub(matrix<type>& B)
{
	if ((sizex != B.sizex) || (sizey != B.sizey)) return false;

	for (int i = 0; i < this->sizey; i++)
		for (int j = 0; j < this->sizex; j++)
			this->val[i][j] -= B.val[i][j];
	return false;
}

template<class type> bool matrix<type>::mul(matrix<type>& B)
{
	if (this->sizex != B.sizey) return false;
	this = mul(*this, B);
	return true;
}

template<class type> bool matrix<type>::out()
{
	for (int i = 0; i < sizey; i++)
	{
		for (int j = 0; j < sizex; j++)
			std::cout << val[i][j] << " ";
		std::cout << std::endl;
	}
	return true;
}

template<class type> bool matrix<type>::fin(const char * filename)
{
	std::fstream in(filename, std::fstream::in);
	unsigned int x = 0;
	unsigned int y = 0;
	in >> x;
	in >> y;
	if (sizex != x || sizey != y)setsize(x, y);
	for (int i = 0; i < sizey; i++)
	{
		for (int j = 0; j < sizex; j++)
			in >> val[i][j];
	}
	in.close();
	return true;
}

template<class type> bool matrix<type>::fout(const char * filename)
{
	std::fstream out(filename, std::fstream::out);
	out << sizex << "\t" << sizey << std::endl;
	for (int i = 0; i < sizey; i++)
	{
		for (int j = 0; j < sizex; j++)
			out << val[i][j] << "\t";
		out<< std::endl;
	}
	out.close();
	return true;
}


template<class type> matrix<type> matrix<type>::add(matrix<type>& A, matrix<type>& B)
{
	if ((sizex != B.sizex) || (sizey != B.sizey)) return matrix<type>();
	matrix<type>S;
	S = A;
	S += B;
	return matrix<type>(S);
}

template<class type> matrix<type> matrix<type>::dif(matrix<type>& A, matrix<type>& B)
{
	if ((sizex != B.sizex) || (sizey != B.sizey)) return matrix<type>();
	matrix<type>S;
	S = A;
	S -= B;
	return matrix<type>(S);
}

template<class type> matrix<type> matrix<type>::mul(type a)
{
	matrix<type>S;
	S.setsize(sizex, sizey);
	for (int i = 0; i < sizey; i++)
		for (int j = 0; j < sizex; j++)
			S.val[i][j] = val[i][j]*a;
	return matrix<type>(S);
}

template<class type> matrix<type> matrix<type>::mul(matrix<type>& A, matrix<type>& B)
{
	if (A.sizex != B.sizey) return matrix<type>();
	matrix<type>C(B.sizex, A.sizey);

	for (int i = 0; i < C.sizey; i++) 
		for (int j = 0; j < C.sizex; j++)
		{
			C.val[j][i] = (type)0;
			for (int z = 0; z < B.sizey; z++)
					C.val[j][i] += A.val[i][z] * B.val[z][j];
		}
	return matrix<type>(C);
}

template<class type> matrix<type> matrix<type>::div(type a)
{
	matrix<type>S;
	S.sizex = sizex;
	S.sizey = sizey;
	for (int i = 0; i < sizey; i++)
		for (int j = 0; j < sizex; j++)
			S.val[i][j] = val[i][j] / a;
	return matrix<type>(S);
}

template<class type> int matrix<type>::geterror()
{
	return erf;
}
