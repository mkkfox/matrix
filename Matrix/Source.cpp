#include "matrix.h"

int main()
{
	bool test;
	matrix<double> k;
	k.setsize(3, 2);
	k = 0.0;
	k.setval(1, 1, 5);
	matrix<double> n(1, 4);
	matrix<double> x;
	matrix<double> f;
	n = 1;
	n >> "test.txt";
	n.out();
	n << "test.txt";
	n.out();
	std::cout << n.at(2, 3);
	test = n == k;
	n = k;
	x = k;
	x += k;
	test = n == k;
	f = n + k;
	test = (f == x);
	x = n.t();
	matrix<double> I(5);
	I = I * 2.0;
	I.out();
	I >> "out.txt";
	matrix<double> d;
	d.fin("out.txt");
	matrix<double> c(2, 2);
	c = 1;
	c.set(0, 0, 7);
	c.set(0, 1, 3);
	c.set(1, 0, 4);
	c.set(1, 1, 2);
	
	d=c.inv();
	c.out();
	d.out();
	I = c*d;
	I.out();
	c << "out.txt";
	std::cout << std::endl;
	c.out();
	std::cout << std::endl;
	c.c_swap(0, 1);
	c.out();
	std::cout << std::endl;
	
	c.r_swap(1, 3);
	c.out();
	n << "in.txt";
	c = n.inv();
	c >> "inv.txt";
	d = c*n;
	d >> "out.txt";

	matrix<int>ch(0, 0);
	matrix<int>u(0, 0);
	u = ch*u;
	matrix<float> id(5);
	float y = 2;
	y = id.get(-2, 0);
	u.setsize(2, 2);
	u = 1;
	u.out();
	u.inverse();
	u.out();

	ch << "multest.txt";
	u = ch.t();
	u >> "u.txt";
	u = ch*u;
	u >> "mulout.txt";
	return 0;
}